import React, { useEffect, useState } from "react";
import Octo from "../../assests/OCTO.png";
import tick from "../../assests/tick.png";
import circle from "../../assests/circle.png";

function mailconfirm() {
  return (
    <>
      <div className="d-flex">
        <div className="col-md-4 login-bg d-flex align-items-center pb-5">
          <div className="login-octo">
            <img src={Octo} />
            <p className="lorem">Just what the doctor ordered !</p>
            
          </div>
        </div>

        <div className="col-md-8 ">
          <div className=" login-box-center ">
            <div className="logged">
              <div className="tick">
                <img src={tick} />
              </div>
              <div className="circle">
                <img src={circle} />
              </div>
              <p className="Confirmation">Mail Confirmation</p>
              <p className="emailed">We have emailed your password reset link.</p>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default mailconfirm;
