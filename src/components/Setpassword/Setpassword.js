import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { Form, Button } from "react-bootstrap";
import Octo from "../../assests/OCTO.png";
import { AiFillEye, AiFillEyeInvisible } from "react-icons/ai";

function Setpassword() {
  const [pwd, setPwd] = useState("");
  const [email, setEmail] = useState("");
  const [isRevealPwd, setIsRevealPwd] = useState(false);

  const initialValues = { email: "", password: "" };
  const [formValues, setFormValues] = useState(initialValues);
  const [formErrors, setFormErrors] = useState({});
  const [isSubmit, setIsSubmit] = useState(false);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
    console.log(formValues);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    setFormErrors(validate(formValues));
  };

  useEffect(() => {
    console.log(formErrors);
    if (Object.keys(formErrors).length === 0 && isSubmit) {
      console.log(formValues);
    }
  }, [formErrors]);

  const validate = (values) => {
    const errors = {};
    if (!values.email) {
      errors.email = "Email is required";
    }
    if (!values.Password) {
      errors.Password = "Password is required";
    }
    return errors;
  };
  return (
    <>
      <div className="d-flex">
        <div className="col-md-4 login-bg d-flex align-items-center pb-5">
          <div className="login-octo ">
            <img src={Octo} />
            <p className="lorem">Just what the doctor ordered !</p>
          </div>
        </div>

        <div className="col-md-8 ">
          <div className=" login-box-center ">
            <div className="login">
              <p className="set-password">Set new password</p>
              <p className="new-password mb-5">
                Your new password must be different to previously used
                passwords.
              </p>
              <Form className="email ">
              <Form.Group className="password mb-4" controlId="password">
                  <Form.Control
                    type={isRevealPwd ? "text" : "password"}
                    value={pwd}
                    name="password"
                    placeholder="Password"
                    onChange={handleChange}
                    value={formValues.password}
                  />
                  <span
                    className="eye"
                    onClick={() => setIsRevealPwd((prevState) => !prevState)}
                  >
                    {isRevealPwd ? <AiFillEye /> : <AiFillEyeInvisible />}
                  </span>
                  <p className="text-danger mb-0"> {formErrors.Password} </p>
                </Form.Group>

                <Form.Group className="password mb-0" controlId="password">
                  <Form.Control
                    type={isRevealPwd ? "text" : "password"}
                    value={pwd}
                    name="password"
                    placeholder="Confirm Password"
                    onChange={handleChange}
                    value={formValues.password}
                  />
                  <span
                    className="eye"
                    onClick={() => setIsRevealPwd((prevState) => !prevState)}
                  >
                    {isRevealPwd ? <AiFillEye /> : <AiFillEyeInvisible />}
                  </span>
                  <p className="text-danger mb-0"> {formErrors.Password} </p>
                </Form.Group>

                <Button
                  variant="primary"
                  type="submit"
                  className="w-100 login-btn mt-4"
                >
                 <Link Link  to ="/Passwordreset" className="login-btn">Reset password </Link> 
                </Button>
                <div className="new-user mt-4">
                  <p>
                    Back to log in?{" "}
                    <span>
                      <Link  to ="/" href="">Log in</Link>
                    </span>
                  </p>
                </div>
              </Form>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default Setpassword;
