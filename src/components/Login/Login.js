import React, { useEffect, useState } from "react";
import { AiFillEye, AiFillEyeInvisible } from "react-icons/ai";
import { Form, Button } from "react-bootstrap";
import Octo from "../../assests/OCTO.png";
import { Link } from "react-router-dom";

function Login() {
  const [pwd, setPwd] = useState("");
  const [email, setEmail] = useState("");
  const [isRevealPwd, setIsRevealPwd] = useState(false);

  const initialValues = { email: "", password: "" };
  const [formValues, setFormValues] = useState(initialValues);
  const [formErrors, setFormErrors] = useState({});
  const [isSubmit, setIsSubmit] = useState(false);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
    console.log(formValues);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    setFormErrors(validate(formValues));
  };

  useEffect(() => {
    console.log(formErrors);
    if (Object.keys(formErrors).length === 0 && isSubmit) {
      console.log(formValues);
    }
  }, [formErrors]);

  const validate = (values) => {
    const errors = {};
    if (!values.email) {
      errors.email = "Email is required";
    }
    if (!values.Password) {
      errors.Password = "Password is required";
    }
    return errors;
  };

  return (
    <>
      <div className="d-flex ">
        <div className="col-md-4 login-bg">
          <div className="login-octo">
            <img src={Octo} />
            <p className="lorem">Just what the doctor ordered !</p>
            <div className="login-content">
              <p className="welcome"> Welcome to Octo</p>
              <p className="lorem">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                enim ad minim
              </p>
            </div>
          </div>
        </div>

        <div className="col-md-8 ">
          <div className=" login-box-center ">
            <div className="login">
              <p>Login</p>
              <Form className="email " onSubmit={handleSubmit}>
                <Form.Group controlId="email" className="mb-4">
                  <Form.Control
                    type="text"
                    name="email"
                    // onChange={(e) => setEmail(e.target.value)}
                    placeholder="Email*"
                    onChange={handleChange}
                    value={formValues.email}
                  />
                  <p className="text-danger mb-0"> {formErrors.email} </p>
                </Form.Group>

                <Form.Group className="password mb-0" controlId="password">
                  <Form.Control
                    type={isRevealPwd ? "text" : "password"}
                    value={pwd}
                    name="password"
                    placeholder="Password*"
                    onChange={handleChange}
                    value={formValues.password}
                  />
                  <span
                    className="eye"
                    onClick={() => setIsRevealPwd((prevState) => !prevState)}
                  >
                    {isRevealPwd ? <AiFillEye /> : <AiFillEyeInvisible />}
                  </span>
                  <p className="text-danger mb-0"> {formErrors.Password} </p>
                </Form.Group>
                <Link to ="/Forgot" className="forgot float-end mb-3">
                  <a href="">Forget password?</a>
                </Link>

                <Button
                  variant="primary"
                  type="submit"
                  className="w-100 login-btn "
                >
                  Login
                </Button>
                <div className="new-user mt-4">
                <p>
                  New user?{" "}
                  <span>
                    <a href="">Sign up</a>
                  </span>
                </p>
              </div>
              </Form>
              
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default Login;
