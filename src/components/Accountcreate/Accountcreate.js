import React, { useEffect, useState } from "react";
import Octo from "../../assests/OCTO.png";
import addFile from "../../assests/addFile.png";
import { Button, Form, Row, Col } from "react-bootstrap";
import { Link } from "react-router-dom";

function Accountcreate() {
  return (
    <>
      <div className="d-flex">
        <div className="col-md-4 login-bg d-flex align-items-center pb-5">
          <div className="login-octo">
            <img src={Octo} />
            <p className="lorem">Just what the doctor ordered !</p>
          </div>
        </div>

        <div className="col-md-8 ">
          <div className=" account-box-center ">
            <div className="log-reset ">
              <p className="mt-3 mb-4 account-text">Create account</p>
              <Form className="create-content">
                <Form.Group
                  // as={Row}
                  className="mb-3 retailStore-text"
                  controlId="formPlaintextPassword"
                >
                  <Form.Control
                    type="text"
                    name="email"
                    placeholder="Pharmacy name"
                    className="inline-text"
                  />
                </Form.Group>
                <Form.Group
                  // as={Row}
                  className="mb-3 retailStore-text"
                  controlId="formPlaintextPassword"
                >
                  <Form.Control
                    type="text"
                    name="email"
                    placeholder="Competent person name"
                    className="inline-text"
                  />
                </Form.Group>
                <Form.Group
                  // as={Row}
                  className="mb-3 retailStore-text"
                  controlId="formPlaintextPassword"
                >
                  <Form.Control
                    type="text"
                    name="email"
                    placeholder="Email"
                    className="inline-text"
                  />
                </Form.Group>
                <Form.Group
                  // as={Row}
                  className="mb-3 retailStore-text"
                  controlId="formPlaintextPassword"
                >
                  <Form.Control
                    type="text"
                    name="email"
                    placeholder="Password "
                    className="inline-text"
                  />
                </Form.Group>
                <Form.Group
                  // as={Row}
                  className="mb-3 retailStore-text"
                  controlId="formPlaintextPassword"
                >
                  <Form.Control
                    type="text"
                    name="email"
                    placeholder="Mobile number"
                    className="inline-text"
                  />
                </Form.Group>
                <Form.Group className="mb-3 " controlId="formPlaintextPassword">
                  <div className="d-flex gap-3">
                    <div>
                      <Col
                        sm="12"
                        className="d-flex flex-column justify-content-center icon-box p-2 "
                      >
                        <label htmlFor="upload-button" className="w-100">
                          {" "}
                          <img
                            src={addFile}
                            alt="GST"
                            className="ml-3 cursor-pointer"
                          />{" "}
                        </label>
                        <small className="inline-text"> GSTIN copy </small>
                      </Col>

                      <div>
                        <label htmlFor="upload-button">
                          <>
                            <small className="color-text text-center remove-txt">
                              {" "}
                              Upload file{" "}
                            </small>
                          </>
                        </label>
                        <input
                          type="file"
                          id="upload-button"
                          style={{ display: "none" }}
                        />
                        <br />
                      </div>
                    </div>
                    <div className="d-flex flex-column justify-content-center">
                      <Col
                        sm="12"
                        className="d-flex flex-column justify-content-center icon-box  p-2 "
                      >
                        <label htmlFor="upload-button">
                          {" "}
                          <img
                            src={addFile}
                            alt="Drug"
                            className="ml-3 cursor-pointer"
                          />{" "}
                        </label>
                        <small className="inline-text"> Drug license </small>
                      </Col>

                      <div>
                        <label htmlFor="upload-button">
                          <>
                            <small className="color-text text-center remove-txt ml-5">
                              {" "}
                              Upload file{" "}
                            </small>
                          </>
                        </label>
                        <input
                          type="file"
                          id="upload-button"
                          style={{ display: "none" }}
                        />
                        <br />
                      </div>
                    </div>
                  </div>
                </Form.Group>
              </Form>
              <Button className="float-right p-2 pl-5 pr-5 mb-3 ml-3 Next modalNextBtnHoverBg ">
                <Link to="/Maplocation" className="Next">Next</Link>
              </Button>
              <div className="new-user ">
                <p>
                  Already user?
                  <span>
                    <a href=""> Log in</a>
                  </span>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default Accountcreate;
