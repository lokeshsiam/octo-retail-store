import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { Form, Button } from "react-bootstrap";
import Octo from "../../assests/OCTO.png";

function Forgot() {
  return (
    <>
      <div className="d-flex">
        <div className="col-md-4 login-bg d-flex align-items-center pb-5">
          <div className="login-octo ">
            <img src={Octo} />
            <p className="lorem">Just what the doctor ordered !</p>
          </div>
        </div>

        <div className="col-md-8 ">
          <div className=" login-box-center ">
            <div className="login">
              <p>Forgot Password?</p>
              <p className="fg-password mb-5">
                No worries! just enter your email and we’ll send you a reset
                password link
              </p>
              <Form className="email">
                <Form.Group controlId="email" className="mb-4">
                  <Form.Control
                    type="text"
                    name="email"
                    placeholder="Enter your e-mail ID*"
                  />
                </Form.Group>

                <Button
                  variant="primary"
                  type="submit"
                  className="w-100 login-btn "
                >
                  <Link to="/Mailconfirm" className="login-btn">
                    Submit
                  </Link>
                </Button>
                <div className="new-user mt-4">
                  <p>
                    Back to log in?{" "}
                    <span>
                      <Link to="/Setpassword" href="">
                        Log in
                      </Link>
                    </span>
                  </p>
                </div>
              </Form>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default Forgot;
