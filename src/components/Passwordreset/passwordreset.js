import React, { useEffect, useState } from "react";
import Octo from "../../assests/OCTO.png";
import tick from "../../assests/tick.png";
import circle from "../../assests/circle.png";
import { Form, Button } from "react-bootstrap";
import { Link } from "react-router-dom";

function passwordreset() {
  return (
    <>
      <div className="d-flex">
        <div className="col-md-4 login-bg d-flex align-items-center pb-5">
          <div className="login-octo">
            <img src={Octo} />
            <p className="lorem">Just what the doctor ordered !</p>
          </div>
        </div>

        <div className="col-md-8 ">
          <div className=" login-box-center ">
            <div className="logged-reset">
              <div className="tick">
                <img src={tick} />
              </div>
              <div className="circle">
                <img src={circle} />
              </div>
              <p className="Confirmation">Password reset</p>

              <p className="emailed mb-0">
                Your password has been successfully reset.
              </p>
              <p className="emailed ">Click below button to log in manually</p>

              <Button
                variant="primary"
                type="submit"
                className=" back-to-login mt-2"
              >
                <Link to = "/Accountcreate"  className="login-btn">Back to login</Link>
              </Button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default passwordreset;
