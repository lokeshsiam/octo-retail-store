import "./App.css";

import { BrowserRouter, Routes, Route, Outlet, Link } from "react-router-dom";
import Loginpage from "../src/components/Login/Login";
import Forgotpage from "../src/components/Forgot/Forgot";
import Confirmpage from "../src/components/Mailconfirm/mailconfirm"
import Newpassword from "../src/components/Setpassword/Setpassword"
import Resetpassword from "../src/components/Passwordreset/passwordreset"
import Createaccount from "../src/components/Accountcreate/Accountcreate"
import Locationpage from "../src/components/Maplocation/Location"

function App() {
  return (
    <>
      <BrowserRouter>
        <Routes>
          <Route exact path="/" element={<Loginpage />} />
          <Route exact path="Forgot" element={<Forgotpage />} />
          <Route exact path="Mailconfirm" element={<Confirmpage />} />
          <Route exact path="Setpassword" element={<Newpassword />} />
          <Route exact path="Passwordreset" element={<Resetpassword />} />
          <Route exact path="Accountcreate" element={<Createaccount />} />
          <Route exact path="Maplocation" element={<Locationpage />} />

        </Routes>
      </BrowserRouter>
    </>
  );
}

export default App;
