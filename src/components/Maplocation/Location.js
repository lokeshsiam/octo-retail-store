import React from 'react'
import Octo from "../../assests/OCTO.png";

function Location() {
  return (
    <>
    <div className="d-flex">
        <div className="col-md-4 login-bg d-flex align-items-center pb-5">
          <div className="login-octo">
            <img src={Octo} />
            <p className="lorem">Just what the doctor ordered !</p>
          </div>
        </div>
        </div>
    </>
  )
}

export default Location